const express = require('express');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
const app = express();
app.use(fileUpload());

//CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/upload', (req, res) => {
  console.log("Request on /upload");

  if (!req.files) {
    return res.status(400).json({ msg: 'No files uploaded' });
  }
  const file = req.files.file;
  console.log(file);
  file.mv(`${__dirname}/uploads/${file.name}`, err => {
    if (err) {
      console.error(err);
      return res.status(500).json({ errors: err });
    }
  })
  return res.status(200).json({ msg: `File '${file.name}' uploaded.` });
});

app.get('/files/:name', (req, res) => {
  var options = {
    root: __dirname + '/uploads/',
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  };
  var fileName = req.params.name;
  res.sendFile(fileName, options, function (err) {
    if (err) {
      next(err);
    } else {
      console.log('Sent:', fileName);
    }
  });
})

app.get('/files', (req, res) => {
  fs.readdir('./uploads/', {}, (err, data) => {
    if (err) throw err;
    res.status(200).json({ files: data })
  });
})

app.delete('/files', (req, res) => {
  return fs.readdir('./uploads', (err, files) => {
    if (err) {
      return res.status(500).json({ errors: err });
    }
    for (const file of files) {
      fs.unlink(path.join('./uploads', file), err => {
        if (err) {
          return res.status(500).json({ errors: err });
        }
      });
    }
    return res.status(200).json({ msg: 'All files removed' });
  });
})

app.listen(5000, () => {
  console.log('Server listening on port 5000');
})