import React, { useState, useEffect } from 'react';
import { Container, Header, Card, Image, Icon, Button } from 'semantic-ui-react';
import UploadButton from './components/UploadButton';
import 'semantic-ui-css/semantic.min.css'
import styles from './App.module.css';
import { getFiles } from './api/helper';

const newDocumentStyle = {
  border: '2px solid greenyellow'
}

function App() {
  const [documents, setDocuments] = useState([]);
  const [isNewDocument, setIsNewDocument] = useState(false);

  useEffect(() => {
    getDocuments();
  }, []);

  useEffect(() => {
    if (isNewDocument) {
      setTimeout(() => setIsNewDocument(false), 2000);
    }
  }, [isNewDocument])

  const getDocuments = () => {
    getFiles().then((res) => {
      setDocuments(res.data.files);
    })
  }

  const handleSuccessUpload = (fileName) => {
    setDocuments([fileName, ...documents]);
    setIsNewDocument(true);
  }
  return (
    <>
      <Container className={styles.App} textAlign='center'>
        <Header as='h1'>
          Files manager.
        </Header>
        <UploadButton handleSuccessUpload={handleSuccessUpload} />
      </Container>

      <Container textAlign='center' style={{ marginTop: '30px' }}>
        {
          !!documents.length && <Button negative>DON'T TOUCH ME</Button>
        }
        <Card.Group textAlign="center" centered style={{ marginTop: '0px' }}>
          {
            documents.map((name, idx) => (

              <Card style={isNewDocument && idx === 0 ? newDocumentStyle : null}>
                <Image src='https://react.semantic-ui.com/images/wireframe/image.png' />
                <Card.Content>
                  <Card.Header>{name}</Card.Header>
                </Card.Content>
                <Card.Content extra>
                  <Icon name='clock' />
                  12/04/19
                </Card.Content>
              </Card>

            ))
          }
        </Card.Group>
      </Container>
    </>
  );
}

export default App;
