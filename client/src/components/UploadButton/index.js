import React, { useState } from 'react';
import { Input, Button } from 'semantic-ui-react';
import styles from './UploadButton.module.css';
import { postFile } from '../../api/helper';

function UploadButton({ handleSuccessUpload }) {
  const [file, setFile] = useState(null);

  const handleNewFile = (e) => {
    if (e.target.files.length) {
      setFile(e.target.files[0]);
    }
  }

  const submitFile = () => {
    postFile(file)
      .then(() => {
        setFile(null);
        handleSuccessUpload(file.name);

      })
      .catch(e => {
        console.error(e);
      })
  }

  return (
    <div>
      <Input type='file' placeholder='Select file' action>
        <input type="file" className={styles.hiddenInput} onChange={handleNewFile} />
        <input type="text" defaultValue={!!file ? file.name : ""} />
        <Button positive={!!file} onClick={submitFile}>Upload</Button>
      </Input>
    </div>

  );
}

export default UploadButton;