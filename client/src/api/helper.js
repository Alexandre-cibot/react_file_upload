import axios from 'axios';

const API_URL = 'http://localhost:5000';

export function postFile(file) {
  const formData = new FormData();
  formData.append('file', file);
  return axios.post(`${API_URL}/upload`, formData);
};

export function getFiles() {
  return axios.get(`${API_URL}/files`);
}